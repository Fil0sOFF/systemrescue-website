+++
title = "Configuring SystemRescue"
draft = false
+++

## Overview
SystemRescue comes with options which allows users to change the way it runs.
For example, there are options for controlling which keyboard of layout to use,
whether the system must be run from memory or from the boot device, whether or
not to automatically start the graphical environment, and so on. You can follow
the link to see the list of [boot options](/manual/Booting_SystemRescue/)
which are supported on the command line.

These options are set on the boot command line. To change these options, you can
either manually edit the boot command line from the boot loader at run time, or
you can make the changes persistent by editing the configuration file of the
boot loaders, which are either isolinux if you start in BIOS mode or Grub if you
start in UEFI mode.

## Configuration features
Since SystemRescue version 9.00 there is another way to configure SystemRescue.
You can now edit a YAML file on the boot device in order to configure it. This
configuration file has been introduced to make it easy to make persistent
configuration changes, and these options are effective for both the BIOS mode
and UEFI mode.

The SystemRescue configuration YAML files are located in the `sysrescue.d`
folder located on the boot device.

SystemRescue comes with a default YAML configuration file, so it provides a good
example of such a file, which you can use as a starting point. You can edit it
to replace the options with your own preferences.

These yaml configuration files support multiple scopes. The main scope is called
`global` and is contains general configuration entries. The `autorun` scope
is used to define configuration entries which are used by
[autorun](/manual/Run_your_own_scripts_with_autorun/).

## Configuration entries
Below is an example of a valid yaml configuration file. In this example there
are entries in both the `global`, `autorun`, and `sysconfig` scopes. The
`copytoram` option is enabled so the system is fully copied to memory at boot
time, the `checksum` option is also enabled so the system checks its integrity
at boot time, as well as the `dostartx` option in order to automatically start
the graphical environment. Both the `nofirewall` and `loadsrm` options are left
disabled, so the firewall will not be turned off and SRM modules will not be loaded.
The `setkmap` option is used to configure a french keyboard layout. All these
general options belong to the `global` scope. The `autorun` scope contains
entries to determine how autorun scripts will be executed. These options are
only useful if you wish to use [autorun](/manual/Run_your_own_scripts_with_autorun/)
to run custom scripts automatically.

```
---
global:
    copytoram: true
    checksum: true
    nofirewall: false
    loadsrm: false
    setkmap: "fr-latin1"
    dostartx: true
    dovnc: false
    rootshell: "/bin/bash"
    rootcryptpass: "$6$Y.AolXkpG/Js2Zqx$z7J893qtB7jKn3z39ucbgvpkJ6wTrJ8N0CBVr5cJ.uXugGTMTSjMI7qsSTu4UTFGGKpGyEG/BnYNRE6oZFO4b0"
    rootpass: "MyRootPassword123"
    vncpass: "MyVncPassword456"

autorun:
    ar_disable: false
    ar_nowait: false
    ar_nodel: false
    ar_attempts: 1
    ar_ignorefail: false
    ar_suffixes: "0,1,2,3,4,5"

sysconfig:
    ca-trust:
        example-ca: |
            -----BEGIN CERTIFICATE-----
            MIIDlTCCAn2gAwIBAgIUbB4K7H53E3spHfMtSb0To+Fyb3wwDQYJKoZIhvcNAQEL
            BQAwWjELMAkGA1UEBhMCWFgxFTATBgNVBAcMDERlZmF1bHQgQ2l0eTEcMBoGA1UE
            [...]
            VtbLuXNBNjfcAk1xqTb1j9dMeHDZKV4Imr0W3qfsHnWFqihxGyKJ79Qb2bL1Kquc
            vgI/6+yHyDlw
            -----END CERTIFICATE-----
```

## Status of this feature
At this stage only a few essential options are supported in the configuration
file. Only the options shown is the example above are supported at this stage.
Please refer to [boot options](/manual/Booting_SystemRescue/) to find a full
description of each general option.

The plan is to add support for more options or services in the future. Also you
can take advantage of this mechanism and use these yaml configuration files to
configure your own scripts that are executed from SystemRescue. You should
create new scopes in your configuration files if you plan to do so.

These features are relatively recent, so it is recommended you use the very
latest version of SystemRescue in order to benefit from the latest features
and bug fixes related to the way support for the configuration is implemented.

## How to use yaml configuration files
To be able to configure SystemRescue using this YAML configuration files, you
must edit the existing YAML file located in `sysrescue.d` on the boot device, or
you can create additional YAML files in the same location.

Files must have a `.yaml` extension (not `.yml`), otherwise the file will be
ignored. Also make sure you follow the yaml syntax correctly. Key and values are
separated by colons, not equal signs, and the indentation is very important.

The system reads any file with a `yaml` extension located in the correct folder,
and it is case insensitive, unless the `sysrescuecfg` option is used to add
restrictions. Files are processed in the alphabetical order, so the settings in
the file which come last can override settings which have been defined before.
Default configuration yaml files are provided with file names starting with
numbers so it is easy to control the sequence in which these files are loaded.
It is recommended to keep a similar prefix for any additional file so the order
in which files are loaded remains predictable. You should create custom files
with a higher number if you want to be sure these settings will take precedence
over different settings from default yaml files. Please note that options passed
on the boot command line will take precedence over options in any yaml
configuration file.

The `sysrescuecfg` option can be specified one or multiple times on the boot
command line to restrict which yaml configuration files will be loaded.
Let's say your boot device contains three yaml configuration files:
`100-defaults.yaml`, `301-profile1.yaml` and `302-profile2.yaml`.
If you boot with `sysrescuecfg=100-defaults.yaml` then only the configuration
from this first file will be loaded. Here is another example: If you use
`sysrescuecfg=100-defaults.yaml sysrescuecfg=301-profile1.yaml` then the first
two files will be loaded but the third one will be ignored. This feature is
useful if you want to prepare a single boot device which you can use in different
situation where different configurations are required. For example you may
create one set of configuration files that apply to your servers, and another
set of configuration files that apply to desktops. You can also customize the
boot menus to provide different entries to start the system with different
sets of configurations, so you do not have to type the option manually each time.

The `sysrescuecfg` option can also be used to load remote yaml files over http/https,
as part of a PXE boot, or as part of a boot from a local device. For example if
you specify `sysrescuecfg=100-defaults.yaml sysrescuecfg=http://192.168.1.1/301-settings.yaml`
then it will load `100-defaults.yaml` from the regular location on the local
boot device and then `http://192.168.1.1/301-settings.yaml` from an HTTP server.
All other yaml configuration files will be ignored.

The simplest way to create or edit the configuration file is to install
SystemRescue on a writable device (such as a USB memory stick) with a writable
file system (such as FAT32). If you install SystemRescue on a USB stick using
either `dd` or any other tool which performs a physical copy of the ISO image,
it will not produce a writable file system, and you will not be able to edit the
configuration file on the device. Hence it is highly recommended to install
SystemRescue on a USB memory stick using [rufus](https://rufus.ie/), and you
must choose to use the `ISO mode` rather than the `DD mode` when prompted.

You can also use [sysrescue-customize](/scripts/sysrescue-customize/) to
create a custom version of the SystemRescue ISO image. The customization process
allows you to add your own yaml configuration files (along with other additional
files if you want to) to the ISO image. You can then write your custom ISO image
on any type of boot device, it does not need to contain a writable file system.

## Certification Authority (CA) trust
If you maintain one or more local Certification Authorities (CA), you can add them
as trust anchors to SystemRescue by listing them in the `ca-trust` section of the
YAML file. You must give each CA an individual name within the `ca-trust` section.
That name will be used as file name within the `/etc/ca-certificates/trust-source/anchors/`
directory.

The list of trusted CAs is guaranteed to be updated before 
[autorun scripts](/manual/Run_your_own_scripts_with_autorun/) are downloaded.
So you can download them with https from a server using a certificate signed by
your CA.

## Implementation details
Support for options located in the YAML configuration file are implemented in
multiple places:

The bulk of the processing of the configuration is implemented in the following script:
[sysrescue-configuration.lua](https://gitlab.com/systemrescue/systemrescue-sources/-/blob/master/airootfs/usr/bin/sysrescue-configuration.lua).
This script processes the yaml configuration files available on the local boot device,
as well as the options specified on the boot command line, and it determines the
"effective" configuration. This is a single JSON file, stored in
`/etc/sysrescue/sysrescue-effective-config.json`, which contains a single definition
of each supported option and allows various programs to determine which value is
applicable for a particular option, without having to process all possible sources
of configuration. Multiple scripts such as `sysrescue-initialize` and `sysrescue-autorun`
use the effective configuration file to determine what to do. Python scripts have
built-in support for reading JSON files. Shell script can use the standard `jq`
command to read values from this JSON file.

Some [archiso hooks](https://gitlab.com/systemrescue/systemrescue-sources/-/blob/master/patches/archiso-v43-07-yaml-config.patch)
read the effective configuration to determine the values for options such as
`copytoram`, `checksum` and `loadsrm` which must be used at an early stage
during the boot process, as part of the initramfs.

The [sysrescue-initialize.py](https://gitlab.com/systemrescue/systemrescue-sources/-/blob/master/airootfs/etc/systemd/scripts/sysrescue-initialize.py) script also uses the effective configuration to determine how the system should be
initialized, in the later stage of the boot process.