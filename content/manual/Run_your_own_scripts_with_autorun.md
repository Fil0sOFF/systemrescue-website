+++
title = "Run your own scripts with autorun"
draft = false
aliases = ["/Sysresccd-manual-en_Run_your_own_scripts_with_autorun"]
+++

## Autorun overview
The **autorun feature** allows you to run scripts automatically at
startup of the system. Each autorun script can manage a task. For
example, you can create a backup script that makes a backup of a
database, another for cleaning a system, ...

By default, the autorun script(s) may be copied in the root of the ISO image
or boot device, outside of the squashfs compressed image file, but other sources
are available : local file system, network share, an HTTP/HTTPS server.

The script may be named `autorun` if alone. If many scripts have to be
chained, you may name them `autorun0`, `autorun1`,... . In this case,
specific autorun scripts may be allowed with an `ar_suffixes=` option.
For example, you may add `ar_suffixes=2,3,4` to the boot command line
or yaml configuration file if you want `autorun2`, `autorun3` and `autorun4`
to be executed, and other autorun scripts such as `autorun1` and `autorun5` to
be ignored.

You can use autorun to perform completely automated tasks using SystemRescue.

## Options provided by the autorun

These options are supported to control the behaviour of autorun. They can be
specified either on the boot command line or through
[YAML configuration files](/manual/Configuring_SystemRescue/)

-   **ar\_source=xxx**: place where the autoruns are located. It may be
    the root directory of a device (`/dev/sda1`), an nfs share
    (`nfs://192.168.1.1:/path/to/scripts`), a samba share (`smb://192.168.1.1/path/to/scripts`),
    or an http/https directory (`http://192.168.1.1/path/to/scripts`)..
    Support for HTTPS was added in August 2020 so make sure you use a recent
    version if you need this feature. Please note this parameter contain the
    address of the folder which contains some autorun script. This is not the
    full address of a particular script, as the name of the script will be added
    at the end of the source.
-   **ar\_nowait**: do not wait for a keypress after the autorun scripts
    have been executed.
-   **ar\_ignorefail**: continue to execute the scripts chain even if a
    script has failed (returned a non-zero status)
-   **ar\_nodel**: do not delete the temporary copy of the autorun
    scripts located in `/var/autorun/tmp` after execution
-   **ar\_disable**: completely disable autorun, the simple `autorun`
    script will not be executed
-   **ar\_suffixes=\[0-9\]**: comma separated list of the suffixes corresponding
    to the autorun scripts which must be executed. For instance if you use
    `ar_suffixes=0,2,7` then the following autorun scripts will be executed:
    `autorun0`, `autorun2`, `autorun7`, and scripts with other suffixes will be
    ignored. Use `autoruns=no` to disable all the autorun scripts with a suffix.
    This option was introduced in SystemRescue 9.00 in order to replace the
    `autoruns=` option which was used similarly with older versions.

## Summary of scripts execution rules

At startup, a list of locations is checked against the presence of
autorun files. They are, successively:

-   if the **ar\_source=** parameter was passed at startup, the root
    directory of the given location. Devices are mounted to
    `/mnt/autorun`, allowing writing data into them if the device is not
    write-protected.
    -   Local disk: `ar_source=/dev/sda2` (autorun scripts stored on a file
        system on the corresponding device on the local disk)
    -   NFS shares: `ar_source=nfs-server:/nfs/exported/directory`
    -   Samba shares: `ar_source=//samba-server/share`
        (recommended without user/password).
    -   Http/https server: `ar_source=https://web-server/adminscripts/`
-   the **autorun** folder located at the root of the boot device (since
    SystemRescue 9.00)
-   the **root directory** of the boot device.
-   the superuser home directory (**/root**)
-   the **/usr/share/sys.autorun** directory

If autorun files are found in some location, they are run and the
process ends. Last two locations require rebuilding of SystemRescue
and are to be used for advanced or test purposes.

In each source location, there are two possible modes of operation :

-   simple one : if a shell script named `autorun` is found, it is run
-   more flexible : if `autorun#` scripts are found (\# is a digit from
    0 to 9) and either
    -   `ar_suffixes=` boot parameter was NOT specified, or
    -   `ar_suffixes=` boot parameter value contains \#

Example: With `ar_suffixes=0,1,4` only `autorun0`, `autorun1` and `autorun4`
scripts will be executed if present. Other scripts, such as `autorun2` and
`autorun3` will be ignored. Authorized scripts are run in alphanumeric order.
Whenever a script returns a non-zero code, the processing stops and next scripts
are not run. You can prevent any numbered `autorun\#` script from being executed
by using the `ar_suffixes=no` parameter. This has no effect on the primary
`autorun` script which has no suffix in its file name.

Although it is possible to put both `autorun` and `autorun#` scripts,
this should probably be avoided.

The script can be any script using an installed interpreter like shell, Python, Perl, ...
It should have a shebang (#!) at the beginning to indicate the interpreter to use. 
ELF binaries are also supported.

Scripts without shebang are executed by the POSIX shell (`/bin/sh`). This is deprecated
and a future version of SystemRescue will require all scripts to have a shebang.

Windows end-of-line terminators (`\r\n`) are translated to allow running shell scripts 
written with a MS editor. This is deprecated and future revisions of SystemRescue will
not modify line endings anymore.

## Custom ISO image

If you intend to run autorun scripts from a SystemRecue running from a boot
device, you should consider using the [sysrescue-customize](/scripts/sysrescue-customize/)
script. It provides a very easy way to create a custom ISO image with additional
files such as autorun scripts.

## Troubleshooting

Since SystemRescue 9.00, you can find a general autorun log file in
`/var/log/sysrescue-autorun.log` to determine what has gone wrong. Use this log
file if you have a general autorun issue, for example if your script has not
been executed.

Each autorun script which runs creates its own log file in `/var/autorun/log`.
Use these logs do investigate issues affecting a particular autorun script.

## Examples of autorun scripts

```
#!/bin/bash
(
  ifconfig eth0 | head -n 3
  fdisk -l /dev/sda
) | tee -a /mnt/autorun/report
sync
exit 0
```

In this script we assume that we have passed the floppy-disk as the
source of autorun with `ar_source=fd0` boot-parameter. It will
accumulate infos about eth0 network interface and disk partitions into a
file named `report` on the first floppy drive. The BIOS boot sequence
must specify the CD first, the diskette must be write-enabled, the CD
and diskette must be inserted at startup time. Hint: The parameter `-a`
after the command `tee` has the following effect: the file is not
overwritten but the output is appended.

If network is automatically configured at boot (DHCP), you may send
infos through an NFS or a samba share for example.

Do not forget to give the autorun location at startup. Say that you share
through NFS a writeable directory named `/nfs/backup` on host myserver,
you have to specify: `ar_source=nfs://myserver:/nfs/backup`
at SystemRescue boot prompt and to put in the shared directory an
`autorun` script such as this one :

```
#!/bin/bash
date=$(date +%Y%m%d)
cat /dev/sda | gzip > /mnt/autorun/$date.sda.gz && exit 0
echo $date sda backup error
exit 1
```

If sufficient space is provided in the NFS share, a compressed image of
your first IDE disk will be copied there. Be aware that there may be a
2GB maximum file size when copying across the network or to a FAT
filesystem.

You must ensure that name resolution works if you intend to access the
NFS server by its name instead of its IP address.
