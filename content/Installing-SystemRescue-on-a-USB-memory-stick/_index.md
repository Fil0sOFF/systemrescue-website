+++
weight = 140
title = "Installing SystemRescue on a USB memory stick"
nameInMenu = "Bootable USB"
draft = false
aliases = ["/Sysresccd-manual-en_How_to_install_SystemRescueCd_on_an_USB-stick",
           "/Installing-SystemRescueCd-on-a-USB-stick"]
+++

This page explains how to install SystemRescue on a USB stick. All data on the
memory stick will be deleted so make sure it does not contain any important data.

You need a recent SystemRescue, and a USB stick with enough capacity. You
should use a 2GB memory stick or anything larger. You will have to get your
firmware (BIOS/UEFI) to boot from the USB device before it attempts to boot from
the local disk. This can be configured either in the firmware settings or by
pressing a key at boot time.

This page describes multiple approaches available for installing such a USB
stick. You just need to follow the method you prefer. In any case you need to
start by download SystemRescue-8.03 (or more recent) from
the [download page](/Download/).

## Recommended USB installation method on Windows

If you are running Windows on your computer the recommended installation program
is [rufus](https://rufus.ie/) as it is easy to use and supports both UEFI and
Legacy-BIOS booting methods.

* **Download** [rufus](https://rufus.ie/) and install it on Windows
* **Plug in your USB-stick** and wait a few seconds to allow enough time for the system to detect it
* **Execute Rufus** and select the USB stick in the drop-down list
* **Select the SystemRescue ISO image** that you have downloaded
* **Select 'MBR' partition scheme** as it will be compatible with both BIOS and UEFI
* **Select 'BIOS or UEFI' in target** to get the best compatibility
* **Check the 'volume label' is correct** as it must be set to <code>RESCUEXYZ</code> (cf below)
* **Select FAT32 filesystem** as the UEFI boot process only works from FAT file systems
* **Click on the start button** and wait until the operation is complete
* **Choose the 'ISO mode' when prompted** so you get a writable file system

In the previous steps `RESCUEXYZ` refers to the version number, eg: `RESCUE803`
for SystemRescue-8.03. Rufus should automatically use the label which was set
on the ISO filesystem and hence it should set this label automatically on the
USB device. You should not have to change it but you should make sure the label
is correct as this is required for the device to start properly. What matters is
that the label matches the value passed to the `archisolabel` boot option in the
boot loader configuration files on the device (`grubsrcd.cfg` and
`sysresccd_sys.cfg`) so files can be found at the time the system starts from
the USB device.

## Recommended USB installation method on Linux

The recommended tool for installing SystemRescue to a memory stick on Linux
is [usbimager](https://gitlab.com/bztsrc/usbimager/) as it does the job well, it
is very compact, and it comes with minimal dependencies. This program can be
[downloaded](https://gitlab.com/bztsrc/usbimager/raw/binaries/usbimager_1.0.8-x86_64-linux-x11.zip)
and executed without installation. The archive can be extracted using `unzip` and
the program must be run via `sudo` so it can write to the memory stick device.
This program is very simple to use as you just need to select the ISO image and
the destination removable device using the graphical interface.

## Alternative USB installation method on Linux using Fedora Media Writer

An alternative approach is to use Fedora Media Writer if you want to install
SystemRescue to a memory stick using Linux. It will produce a USB stick which
is bootable for both a Legacy BIOS and in UEFI node.

* **Install flatpak** using the official package from your Linux distribution (dnf, apt, pacman, etc)
* **Install Fedora Media Writer** using flatpak as documented on [flathub](https://flathub.org/apps/details/org.fedoraproject.MediaWriter)
* **Plug in your USB stick** and wait a few seconds to allow enough time for the system to detect it
* **Execute Fedora Media Writer** and select `custom image` from the first menu
* **Select the ISO image** that you have previously downloaded
* **Select the USB stick** where you want to install SystemRescue
* **Click on the button** to start the installation and wait for it to complete.

## Alternative USB installation method on Linux using DD

This approach only requires the dd command to copy the ISO image file to the
USB device. Make sure you use the right device with dd as the operation is
destructive if you write to the wrong device.

* **Plug in your USB stick** and wait a few seconds to allow enough time for the system to detect it
* **Unmount the USB stick** if auto-mount is enabled or if it was already mounted
* **Run <code>lsblk</code>** in a terminal to identify the device name for your USB device
* **Run <code>sudo dd if=/path/to/systemrescue-x.y.z.iso of=/dev/sdx status=progress</code>** where `/dev/sdx` is the USB stick

## Alternative USB installation method using Ventoy for Multiboot

Another approach to install Systemrescue makes use of the software [Ventoy](https://www.ventoy.net/en/index.html). Ventoy allows you to create a bootable multi ISO USB drive without the need to reformat every time you want to use a different ISO.

- **Make sure to backup** the contents of your USB stick, before formatting it, if there is any important data.
- **Format your USB drive**.
- **Install Ventoy on your USB drive** according to this [manual](https://www.ventoy.net/en/doc_start.html) either on Linux or Windows. 
- **After installing Ventoy on your USB drive simply place the Systemrescue ISO inside the first partition.** Ventoy will automatically find the ISO and list it for boot if you insert the USB drive for booting.

You can also place the 32 and 64 bit version together on the USB drive, so you can choose between the necessary architecture without the need to reformat. 

## Manual USB installation method on Linux for booting in UEFI mode

Follow this method is you want to boot the USB device in UEFI mode, or if you
want to have a writable file system on the boot device, so it is easy to add
custom files, such as yaml configuration files or autorun scripts for
SystemRescue. The idea is to create a FAT32 partition on the device where the
firmware will find Grub. For the UEFI firmware to use this partition as the EFI
filesystem is must have the right flags in the partition table. The USB device
can have additional partitions.

* **Create a msdos** disklabel on the USB device using a tool such as parted or gparted
* **Create a FAT32 partition** with at least 2GB on the USB device and set <code>RESCUEXYZ</code> as the filesystem label
* **Set the boot and lba flags on the FAT32 partition** using a tool such as parted or GParted
* **Copy all files from the ISO image to the FAT32 partition** from a terminal with both devices mounted

In the previous steps `RESCUEXYZ` refers to the version number, eg: `RESCUE803`
for SystemRescue-8.03.

After you have followed these steps you should check the contents of both
`/boot/grub/grubsrcd.cfg` and `/sysresccd/boot/syslinux/sysresccd_sys.cfg` on
the USB device has the right value for the `archisolabel` boot option. The label
passed to this option needs to match the label of the FAT32 filesystem on the USB
device so it can find the SystemRescue files during the boot time. A mismatch
will cause a boot failure.
